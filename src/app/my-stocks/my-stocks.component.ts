import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-stocks',
  templateUrl: './my-stocks.component.html',
  styleUrls: ['./my-stocks.component.css']
})
export class MyStocksComponent  {
  elements: any = [
    {name: 'BAB', purchase: 50, current: 200, owned: 3000},
    {name: 'SNE', purchase: 60, current: 300, owned: 6000},
    {name: 'MSFT', purchase: 70, current: 400, owned: 9000},
    
  ];

  stockHeaders = ['Name', 'Purchase Price', 'Current Price', 'Amount Owned'];

}
